# Story Organization Modeling

+ goal 1614455361895
+ termbase ref_db.xml
+ actor Stakeholder
+ source Requirements

during which the organizational context is analyzed and the
agents and their goals identified. Any agent may generate its own goals, may operate to achieve goals on the behalf of some other agents, 
may decide to collaborate
with or delegate to other agents for a specific goal, and might clash on some other
ones. 

The resulting goals will then be refined, through interaction with the involved
agents, by hardgoal and softgoal modeling.

