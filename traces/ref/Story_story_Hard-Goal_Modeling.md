# Story GOAL story_Hard-Goal_Modeling

+ goal 1614455376086
+ termbase ref_db.xml

seeks to determine how an agent can achieve a received hardgoal, 
by decomposing it into more elementary subordinate hard-goals, tasks 3 , and
resources 4 . Supported by the REF graphical notation, the analyst and the agent will
work together to understand and formalize how the agent thinks to achieve the goal,
in terms of subordinate hardgoals and tasks that he or she will have to achieve and
perform directly, or indirectly, by passing them to other agents.
