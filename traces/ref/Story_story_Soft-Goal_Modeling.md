# Story GOAL story_Soft-Goal_Modeling

+ goal 1614455389296
+ termbase ref_db.xml

aims at producing the operational definitions of the softgoals,
sufficient to capture and make explicit the semantics that are usually assigned implicitly by the involved agents. 
Unlike for an hard-goal, for a softgoal the
achievement criterium is not, by definition, sharply defined, but implicit in the originator intentions. 
The analyst’s objective during softgoal modeling is to make explicit such intentions, in collaboration with the goal originator. 
---
However, depending
on the issue at hand, and the corresponding role played by the two agents (i.e., the
originator and the recipient) within the organization, also the recipient may be involved 
in the process, to reach a sharply defined achievement criterium upon which
both of them can agree. Again, the analyst and the agents will cooperate through
the support of the REF graphical notation.